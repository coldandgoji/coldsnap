import fse from 'fs-extra'
import path from 'path'
import express from 'express'
import proxy from 'express-http-proxy'
import livereload from 'livereload'
import html from './output/html.js'
import log, { logError } from './log.js'
import { instances, PREVIEW } from '../app.js'

const _server = express()
let server = null
let port = null
let portStart = null
let liveServer = null
let liveServerPort = 35729
let root = null
let _error = null
let embed = ''

/**
 * Starts the main server and livereload server
 */
const start = settings => {
  log( '' )
  log( 'Server' )

  let rootRelative = settings.root ?? './public/'
  root = path.resolve( rootRelative  )
  log( `  serve: ${ rootRelative }` )

  // proxy requests to the netlify directory
  _server.use( '/.netlify/functions/', proxy( 'localhost:9000' ) )

  // locale folders
  const folders = []
  for ( const [ key, value ] of Object.entries( instances ) ) {
    const locale = value.locale
    if ( locale.folder && !folders.includes( locale.folder ) ) {
      folders.push( locale.folder )
    }
  }
  const folderDefault = instances?.default?.locale?.folder ?? folders[ 0 ]

  // route to catch all requests
  _server.get( '*', ( req, res ) => {
    // adds no-cache headers
    res.header( 'Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0' )

    // retrieves the requested path
    let route = req.path
    let id = null

    // shows the error
    if ( route.indexOf( '.' ) === -1 && _error ) {
      return send500( req, res )
    }

    if ( route.indexOf( '/id/' ) === 0 ) {
      id = route.replace( /\/id\//, '' )
    } else {
      // performs additional handling for routes not containing a file extension
      if ( route.indexOf( '.' ) === -1 ) {
        // redirects the root to the default locale
        if ( route === '/' && folderDefault ) {
          return res.redirect( folderDefault )
        }

        // appends /index for the root
        // or for all paths when trailing slashes are being used
        if ( route === '/' ) {
          route = '/index'
        } else if ( folders.length && ( folders.includes( route ) || folders.includes( route.replace( /\/$/, '' ) ) ) ) {
          if ( !route.endsWith( '/' ) ) {
            return res.redirect( route + '/' )
          }
          route += 'index'
        } else if ( settings.trailing !== false && !route.endsWith( '/index' ) ) {
          route += route.endsWith( '/' ) ? 'index' : '/index'
        }
      }
    }

    // builds the file
    const target = id !== null ? { id } : { route }
    if ( html.exists( target ) ) {
      html.build( target )
        .then( string => {
          if ( !string ) {
            if ( _error ) {
              return send500( req, res )
            }
            return send404( req, res )
          } else {
            log( `  route: ${ req.path }` )
          }

          // inserts the livereload embed into all responses
          if ( !PREVIEW ) {
            const bodyTag = '</body>'
            const bodyTagIndex = string.lastIndexOf( bodyTag )
            if ( bodyTagIndex !== -1 ) {
              const before = string.substring( 0, bodyTagIndex )
              const after = string.substring( bodyTagIndex + bodyTag.length, string.length )
              string = before + embed + bodyTag + after
            } else {
              string += embed
            }
          }

          // sends the response
          res.send( string )
        } )
    } else {
      // sends the file if it exists
      if ( fse.pathExistsSync( path.join( root, route ) ) ) {
        res.sendFile( route, { root } )

      // sends a simple 404 message instead
      } else {
        return send404( req, res )
      }
    }
  } )

  // starts the localhost server
  portStart = parseInt( process.env.PORT ?? settings.port )
  port = portStart
  server = _server.listen(
    port,
    () => {
      settings.port = port
      log( `  address: //localhost:${ port }` )

      // starts the livereload server
      if ( !PREVIEW ) {
        // determines the livereload server port
        liveServerPort = liveServerPort + port - portStart

        // creates the livereload server embed
        embed = `\n<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':${ liveServerPort }/livereload.js?snipver=1"></' + 'script>')</script>\n`

        // creates the livereload server
        liveServer = livereload.createServer(
          { port: liveServerPort },
          log( `  livereload: //localhost:${ liveServerPort }` )
        )

        // tells the live reload server to watch only the js and css source files
        // this is because the html rebuild can be very wide reaching (thousands of files)
        // so instead of watching changes on those files, we simply trigger a manual refresh
        // using the `refresh()` method below, after rebuilds that have changed the html
        liveServer.watch( [ root + '/**/*.js', root + '/**/*.css' ] )
      }
    } )
    .on( 'error', err => {
      if ( err.code === 'EADDRINUSE' ) {
        port += 1
        server.listen( port )
      } else {
        logError( err )
      }
    } )
}

/**
 * Sends a 500 server error
 */
const send500 = ( req, res ) => {
  return res.status( 500 ).send( `<style>body { background:#042b35;font-family:monospace; }</style>${ embed }${ _error }` )
}

/**
 * Sends a 404 not found error
 */
const send404 = ( req, res ) => {
  _error = false
  logError( `  route: ${ req.path } - does not exist` )
  _error = null
  res.status( 404 ).send( `${ embed }Not Found` )
}

/**
 * Closes the servers
 */
const close = () => {
  server.close()
  if ( liveServer ) {
    liveServer.close()
  }
}

/**
 * Causes a browser refresh via livereload
 */
const refresh = () => {
  _error = null
  if ( liveServer ) {
    liveServer.refresh( '' )
  }
}

/**
 * Causes the browser to refresh
 * if there is currently an error
 */
const recover = () => {
  if ( _error ) {
    _error = null
    refresh()
  }
}

/**
 * Sets and displays an error message
 */
const error = message => {
  if ( !_error && _error !== false ) {
    _error = message
    if ( liveServer ) {
      liveServer.refresh( '' )
    }
  }
}

/**
 * Exports the public interface
 */
export default {
  start,
  close,
  recover,
  refresh,
  error
}
