import log from '../log.js'

import { initializeApp } from "firebase/app"
import { getDatabase, ref, onValue, get, goOffline } from "firebase/database"

import path from 'path'
import fse from 'fs-extra'
import local from './local.js'
import coldsnap, { config, BUILD, DEVELOP, PREVIEW } from '../../app.js'

export default async ( settings = {} ) => {
  const pathCache = settings.cache
  const pathTimestamp = path.join( process.cwd(), pathCache, 'timestamp' )

  // initializes the data
  let data = {}

  // determines if a fresh load is required
  const live = config.live ?? false
  let fresh = config.fresh
  if ( live ) {
    fresh = true
  }
  let first = true

  // loads the cache timestamp
  let timestamp = false
  try {
    timestamp = await fse.readFile( pathTimestamp, 'utf8' )
  } catch ( error ) {
    fresh = true
  }

  // determines if the cache should be used instead
  // this is used for performing offline builds with a
  // pre-existing cache
  let offline = config.offline
  if ( fresh ) {
    offline = false
  }

  log( '' )
  log( 'Firebase' )

  // loads from firebase
  if ( !timestamp || fresh || ( BUILD && !offline ) ) {
    const firebaseApp = initializeApp( settings.connection )
    const database = getDatabase( firebaseApp, settings.connection.databaseURL )
    const reference = ref( database, settings.path || '' )
    log( `  remote: ${ settings.connection.databaseURL }/${ settings.path || '' }` )
    if ( DEVELOP && live ) {
      log( '  connection: live' )
      await new Promise( ( resolve, reject ) => {
        onValue( reference, snapshot => {
          data = snapshot.val() || {}
          if ( settings.onChange && !first ) {
            settings.onChange( data )
          }
          first = false
          resolve()
        } )
      } )
    } else {
      log( '  connection: single' )

      // retrieve entries with 'get'
      // const snapshot = await get( reference )

      // temporary workaround for broken firebase 'get'
      const snapshot = await new Promise( ( resolve, reject ) => {
        onValue( reference, snapshot => {
          resolve( snapshot )
        } )
      } )

      data = snapshot.val() || {}
      goOffline( database )
    }
  }

  // caches the firebase data
  if ( DEVELOP && !PREVIEW && fresh ) {
    log( `  saved: ${ pathCache }` )
    await fse.emptyDir( path.join( process.cwd(), pathCache ) )
    if ( settings.flat ) {
      Object.keys( data ).forEach( entryId => {
        fse.outputFileSync( path.join( process.cwd(), pathCache, entryId + '.json' ), JSON.stringify( data[ entryId ] ) )
      } )
    } else {
      Object.keys( data ).forEach( contentType => {
        Object.keys( data[ contentType ] ).forEach( entryId => {
          fse.outputFileSync( path.join( process.cwd(), pathCache, contentType, entryId + '.json' ), JSON.stringify( data[ contentType ][ entryId ] ) )
        } )
      } )
    }
    const now = new Date()
    fse.outputFileSync( pathTimestamp, now.toLocaleString() )
  }

  // loads the cached data
  if ( ( DEVELOP && !fresh ) || offline ) {
    log( `  cache: ${ timestamp }` )
    data = await local( {
      paths: [ pathCache ],
      onChange: settings.onChange,
      internal: true
    } )
  }

  return data
}
