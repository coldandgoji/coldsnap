import _ from 'lodash'
import bluebird from 'bluebird'
import chokidar from 'chokidar'
import fse from 'fs-extra'
import glob from 'fast-glob'
import path from 'path'
import { DEVELOP } from '../../app.js'
import server from '../server.js'

export let templates = {}

const load = async settings => {
  templates = {}
  await bluebird.map( settings.source ?? [], async source => {
    if ( typeof source === 'string' ) {
      const folder = path.resolve( source )
      const files = glob.sync( path.join( folder, '*' ) )
      await bluebird.map( files, async filename => {
        let template = await fse.readFile( filename, 'utf8' )
        let key = path.basename( filename, path.extname( filename ) )
        if ( settings.compile && typeof settings.compile === 'function' ) {
          template = settings.compile( template )
        }
        templates[ key ] = template
      } )
    } else {
      _.assign( templates, source )
    }
  } )
}


export default async settings => {
  await load( settings )

  if ( DEVELOP && settings.watch ) {
    const paths = ( settings.watch ?? [] ).filter( item => typeof item === 'string' )
    chokidar.watch(
      paths.map( item => path.resolve( item ) ),
      { ignoreInitial: true }
    )
    .on( 'all', async ( event, path ) => {
      await load( settings )
      settings.onChange( templates )
    } )
  }
}
