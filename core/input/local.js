import _ from 'lodash'
import bluebird from 'bluebird'
import chokidar from 'chokidar'
import fse from 'fs-extra'
import glob from 'fast-glob'
import jsyaml from 'js-yaml'
import path from 'path'

import log from '../log.js'
import { DEVELOP, PREVIEW } from '../../app.js'

const extensions = [ '.yml', '.yaml', '.json' ]

const load = async ( paths, data ) => {
  await bluebird.map( paths, async _path => {
    log( `  source: ${ _path }` )

    // looks for the directory path based on the current working directory
    _path = path.resolve( _path )

    // globs in all the yaml and json files
    const files = glob.sync( path.join( _path, '**/+(*.y*ml|*.json)' ) )

    // cleans up the _path
    _path = _path.replace( new RegExp( '^./' ), '' )
    if ( _path.substr( -1 ) !== '/' ) {
      _path += '/'
    }

    // loops through and loads all of the located files
    await bluebird.map( files, async filePath => {
      const fileExtension = path.extname( filePath )
      const fileString = await fse.readFile( filePath, 'utf8' )
      const fileData = ( fileExtension === '.json' )
        ? JSON.parse( fileString )
        : jsyaml.load( fileString )

      // turns the file path into an object path
      let objPath = filePath.replace( new RegExp( '^' + _path ), '' )
      objPath = objPath.replace( new RegExp( '/', 'g' ), '.' )
      objPath = objPath.replace( new RegExp( '(' + extensions.join( '|' ) + ')$' ), '' )

      // injects the id of the file
      fileData.id = objPath.substr( objPath.lastIndexOf( '.' ) + 1 )

      // saves the file data to the specified object path
      _.setWith( data, objPath, fileData, Object )
    }, { concurrency: 200 } )
  } )

  return data
}

export default async ( settings = {} ) => {
  if ( !settings.internal ) {
    log( '', 'Local' )
  }

  const paths = settings.paths || []
  paths.forEach( _path => {
    fse.ensureDirSync( _path )
  } )

  const result = await load( paths, {} )

  if ( DEVELOP && !PREVIEW && settings.onChange ) {
    paths.forEach( item => log( `  watch: ${ item }` ) )
    chokidar.watch(
      paths.map( item => path.resolve( item ) ),
      { ignoreInitial: true }
    )
    .on( 'all', async ( event, path ) => {
      const data = await load( paths, {} )
      settings.onChange( data )
    } )
  }

  return result
}
