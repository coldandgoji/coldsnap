import log, { logError } from '../log.js'
import { DEVELOP, importer, iterate } from '../../app.js'
import glob from 'fast-glob'
import path, { dirname } from 'path'
import { fileURLToPath } from 'url';
import chokidar from 'chokidar'

const __filename = fileURLToPath( import.meta.url )
const __dirname = dirname( __filename )
const extensions = [ '.js' ]

const load = async settings => {
  let result = {}

  for ( let item of settings.types ) {
    if ( typeof item === 'string' ) {
      try {
        const files = glob.sync( path.join( item, '**/*.js' ) )

        item = item.replace( new RegExp( '^./' ), '' )
        if ( item.substr( -1 ) !== '/' ) {
          item += '/'
        }

        for ( const filePath of files ) {
          let objPath = filePath.replace( new RegExp( '^' + item ), '' )
          objPath = objPath.replace( new RegExp( '/', 'g' ), '.' )
          objPath = objPath.replace( new RegExp( '(' + extensions.join( '|' ) + ')$' ), '' )
          const typeName = objPath.substr( objPath.lastIndexOf( '.' ) + 1 )
          if ( typeName.indexOf( '_' ) !== 0 ) {
            result[ typeName ] = await importer( filePath )
          }
        }
      } catch( error ) {
        logError( 'Load Types', error )
      }
    } else {
      for ( let key in item ) {
        const typeName = key.charAt( 0 ).toLowerCase() + key.slice( 1 )
        result[ typeName ] = item[ key ]
      }
    }
  }

  return result
}

export default async ( settings = {} ) => {
  // adjusts relative paths to absolute paths
  settings.types = settings.types.map( item => {
    return typeof item === 'string'
      ? path.resolve( item )
      : item
  } )

  if ( DEVELOP && settings.onChange ) {
    const paths = settings.types.filter( item => typeof item === 'string' )
    chokidar.watch( paths, { ignoreInitial: true } ).on( 'all', async ( event, path ) => {
      iterate()
      const data = await load( settings )
      settings.onChange( data )
    } )
  }

  return load( settings )
}
