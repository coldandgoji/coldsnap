import chokidar from 'chokidar'
import fse from 'fs-extra'
import glob from 'fast-glob'
import path from 'path'
import log, { logError } from './log.js'
import copy from './output/copy.js'
import server from './server.js'
import { BUILD, DEVELOP, PREVIEW, config } from '../app.js'

const getTimestamp = async file => {
  let timestamp = false
  try {
    timestamp = JSON.parse( await fse.readFile( path.resolve( file.timestamp ), 'utf8' ) )
  } catch ( error ) {
    // timestamp doesn't exist
  }
  return timestamp
}

const getLatestChange = file => {
  let files = []
  file.watch.forEach( _path => {
    if ( path.parse( _path ).ext ) {
      files.push( { name: _path, ctime: fse.statSync( _path ).ctime } )
    } else {
      files = files.concat(
       glob.sync( path.resolve( _path, '**/*' ) )
         .map( name => ( { name, ctime: fse.statSync( name ).ctime } ) )
      )
    }
  } )
  return files.sort( ( a, b ) => b.ctime - a.ctime )[ 0 ] ?? {}
}

// performs a build
const build = async ( pipeline, file, latest ) => {
  try {
    await pipeline.build( file )
    await fse.copy( path.resolve( file.destination ), path.resolve( file.cache ) )
    for ( const keep of file.keep ) {
      await fse.copy( path.resolve( keep.destination ), path.resolve( keep.cache ) )
    }
    await fse.outputFile( path.resolve( file.timestamp ), JSON.stringify( { timestamp: latest.toISOString() } ) )
    if ( DEVELOP && file.error ) {
      delete file.error
      server.recover()
    }
  } catch ( error ) {
    file.error = true
    logError( pipeline.name, error )
  }
}

export default init => {
  return async () => {
    // retrieves the pipeline
    const pipeline = await init()

    // loops through the files provided by the pipeline
    log( '', pipeline.name )
    for ( const file of pipeline.files ) {

      let timestamp = file.destination.replace( /^.\//, '' )
      timestamp = timestamp.replace( new RegExp( '[/.]', 'g' ), '-' )
      const mode = PREVIEW ? 'build' : config.mode
      file.watch = file.watch ?? []
      file.watch.unshift( file.source )
      file.cache = path.join( `./.coldsnap/${ mode }/`, file.destination )
      file.timestamp = `./.coldsnap/${ mode }/${ timestamp }`

      // prepares additional files to keep
      file.keep = file.keep ?? []
      for ( const keep of file.keep ) {
        keep.cache = path.join( `./.coldsnap/${ mode }/`, keep.destination )
      }

      // determines if the initial build needs to happen
      const latest = ( getLatestChange( file ) ).ctime
      const previous = await getTimestamp( file )

      // performs the build
      if ( !config.production && ( !previous || ( latest && latest.getTime() > ( new Date( previous.timestamp ) ).getTime() ) ) ) {
        await build( pipeline, file, latest )

      // copies the cache into the real location
      } else {
        log( `  copied: ${ file.destination }` )
        await fse.copy( path.resolve( file.cache ), path.resolve( file.destination ) )
        for ( const keep of file.keep ) {
          log( `  copied: ${ keep.destination }` )
          await fse.copy( path.resolve( keep.cache ), path.resolve( keep.destination ) )
        }
      }

      // watches for future changes
      if ( DEVELOP && !PREVIEW && file.watch.length ) {
        chokidar.watch( file.watch.map( string => path.resolve( string ) ), { ignoreInitial: true } ).on( 'all', async function ( event, path ) {
          const latest = ( getLatestChange( file ) ).ctime
          await build( pipeline, file, latest )
        } )
        file.watch.forEach( string => {
          log(  `  watch: ${ string }` )
        } )
      }

    }
  }
}
