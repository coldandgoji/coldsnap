import _ from 'lodash'
import log, { logError } from './log.js'
import minimist from 'minimist'

export default ( _config = {} ) => {
  // retrieves the arguments
  const argv = minimist( process.argv.slice( 2 ) )
  delete argv._

  // determines the 'content'
  let content = 'published'
  if ( [ 'published', 'draft' ].includes( argv.content ) ) {
    content = argv.content
  }
  if ( argv.draft ) {
    content = 'draft'
    delete argv.draft
  }
  argv.content = content

  // determines the 'mode'
  let mode = 'develop'
  if ( [ 'develop', 'build' ].includes( argv.mode ) ) {
    mode = argv.mode
  }
  if ( argv.develop ) {
    mode = 'develop'
    delete argv.develop
  }
  if ( argv.build ) {
    mode = 'build'
    delete argv.build
  }
  argv.mode = mode

  log( '' )
  log( 'Options' )

  // handles locales if they're provided in the config
  // or if one has been specified as an argument
  let locale = null
  let locales = null
  let singleInstance = true
  argv.locale = argv.locale ?? process.env.LOCALE
  if ( _config.locales || argv.locale ) {
    argv.locale = argv.locale
    if ( !argv.locale ) {
      singleInstance = false
      argv.locale = 'default'
    }
    locales = _config.locales

    if ( !locales.default ) {
      logError( '  If providing locales you must provide a `default` locale' )
      process.exit( 1 )
    }

    // determines the selected locale
    locale = locales[ argv.locale ]
    if ( !locale ) {
      logError(
        `  Could not find locale config for: ${ argv.locale }`,
        '  Please set one in your config.js'
      )
      process.exit( 1 )
    }

    // ensures the default locale has an empty folder
    if ( locales.default.folder === undefined ) {
      locales.default.folder = ''
    }

    // copies the default values into all other locales
    _.each( locales, ( _locale, key ) => {

      // copy in any missing values specified within the default
      if ( key !== 'default' ) {
        Object.keys( locales.default ).forEach( param => {
          if ( _locale[ param ] === undefined ) {
            _locale[ param ] = locales.default[ param ]
          }
        } )
      }

      // adjusts the folder into a standard format
      // with a leading slash and no trailing slash
      if ( _locale.folder !== '' ) {
        if ( !_locale.folder.startsWith( '/' ) ) {
          _locale.folder = '/' + _locale.folder
        }
        if ( _locale.folder.endsWith( '/' ) ) {
          _locale.folder = _locale.folder.slice( 0, -1 )
        }
      }

      // ensures every locale has the prerequisite values
      if ( !_locale.field || !_locale.type || !_locale.currency || !_locale.remote ) {
        logError(
          `  Malformed locale config for: ${ key }`,
          '  Please check the config and ensure it has...',
          '    - `field`',
          '    - `type`',
          '    - `currency`',
          '    - `remote`'
        )
        process.exit( 1 )
      }
    } )

    // removes locales that do not match the single requested instance
    if ( singleInstance ) {
      locales = {
        default: locales[ argv.locale ]
      }
    }
  }

  // processes the arguments
  const args = Object.keys( argv ).sort()
  if ( args.length ) {
    args.forEach( key => {
      if ( key !== '_' ) {
        log( `  ${key}: ${ argv[ key ] }` )
      }
    } )
  }

  // handles default config
  if ( !argv.locale ) {
    argv.locale = 'default'
  }
  locales = locales ?? { default: {} }
  locale = locale ?? {}

  // builds and stores the configuration
  return {
    config: argv,
    locale,
    locales
  }
}
