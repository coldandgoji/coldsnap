import _ from 'lodash'
import path from 'path'
import fse from 'fs-extra'
import log from '../log.js'
import chokidar from 'chokidar'
import { DEVELOP, PREVIEW } from '../../app.js'
import server from '../server.js'

export default async ( settings = {} ) => {
  if ( !settings.silent ) {
    log(
      '',
      'Copy',
      `  source: ${ settings.source }`,
      `  destination: ${ settings.destination }`,
      `  exclude:`
    )
    settings.exclude.forEach( exclude => {
      log( `    ${ exclude }` )
    } )
  }

  // paths to exclude from copying
  const exclude = _.map( settings.exclude, item => path.join( settings.source, item ) )

  // recursively copies all files from the source to the destination
  const copy = async () => {
    let count = 0
    await fse.copy(
      settings.source,
      settings.destination,
      {
        filter: ( source, destination ) => {
          // skip over the initial 'exact folder'
          if ( source === settings.source ) {
            return true
          }

          // checks the path against all excluded paths
          let excluded = false
          exclude.some( string => {
            if ( source.indexOf( string ) === 0 ) {
              excluded = true
              return true
            }
          } )

          // skips over the path if it was discovered
          // within the excluded paths
          if ( excluded ) {
            return false
          }

          // returns true so this file does get copied
          if ( destination.indexOf( '.' ) !== -1 ) {
            count++
          }
          return true
        }
      }
    )

    // logs completion of the copy
    log( `  copied ${ count } ${ count === 1 ? 'file' : 'files' }` )
  }
  await copy()

  // watches for changes to the files
  if ( DEVELOP && !PREVIEW ) {
    const exclude = _.map( settings.exclude, item => path.join( settings.source, item ) )
    chokidar.watch( settings.source, { ignored: exclude, ignoreInitial: true } ).on( 'all', async ( event, path ) => {
      await copy()
      server.refresh()
    } )
    log( `  watch: ${ settings.source }` )
  }
}
