import _ from 'lodash'
import bluebird from 'bluebird'
import glob from 'fast-glob'
import path from 'path'
import fse from 'fs-extra'
import log, { logError } from '../log.js'
import { templates } from '../input/templates.js'
import { instances, BUILD } from '../../app.js'

const regexRootRelativeHref = new RegExp( 'href="/(?!/)', 'g' )
const regexEndsWithSlash = new RegExp( '/$' )
const regexStartsWithSlash = new RegExp( '^/' )

let _init = null
let settings = null
let ids = {}
let routes = {}

/**
 * Prepares all output routes ready for later building
 */
const prepare = async instance => {
  // retrieves the source data for this instance
  let source = settings.source ?? []
  if ( typeof source === 'function' ) {
    source = source( instance ) ?? []
  }

  // loops through all the provided source data
  source.forEach( ( entry, index ) => {
    // ensures the entry has a template
    entry.template = entry.template ?? 'default'

    // ensures the entry has data
    if ( !entry.data ) {
      const message = `  The html source at index #${ index } has no data`
      throw new Error( entry.id ? message + `, the id is ${ entry.id }` : message )
    }

    // prepares each piece of data for this entry
    for ( const item of Object.values( entry.data ) ) {
      // determines the file output location
      let route = typeof entry.url === 'function' ? entry.url( item ) : item.url

      // ensures that a route for output has been provided
      if ( !route ) {
        logError(  '  The supplied data does not provide a `url` for output', item )
      }

      // removes any trailing slash from the url
      route = route.replace( regexEndsWithSlash, '' )

      // adds a subfolder when trailing slashes are being used
      // also appends to /index when no route is available (e.g. the root index)
      if ( settings.trailing !== false || route === '' ) {
        route += '/index'
      }

      // adds the folder for the route
      if ( instance.locale.folder ) {
        route = instance.locale.folder + route
      }

      // creates the reference
      const reference = {
        template: entry.template,
        data: item,
        mode: entry.mode,
        instance
      }

      // indexes by route
      routes[ route ] = reference

      // indexes by id
      if ( item.id ) {
        ids[ item.id ] = reference
      }
    }
  } )
}

/**
 * Determines if the given target exists
 */
const exists = target => {
  if ( target.id ) {
    return !!ids[ target.id ]
  }
  return !!routes[ target.route ]
}

/**
 * Builds the file at the selected route (or id) and returns the output
 */
const build = async target => {
  // retrieves the file from the cache
  const file = target.id ? ids[ target.id ] : routes[ target.route ]

  // provides an error when the file does not exist
  if ( !file ) {
    if ( BUILD ) {
      logError( `  ${ target.route ?? target.id } does not exist` )
    }
    return false
  }

  try {
    // retrieves the template
    const template = templates[ file.template ] ?? file.template

    // stores the currently processing item
    // so it can be easily referenced elsewhere
    file.instance.item = file.data

    // performs the build
    let string = await settings.build( file.data, template, file.mode, file.instance )

    // replaces all root relative urls within the output
    // if this particular locale is being output to a folder
    if ( file.instance.locale.folder ) {
      string = string.replaceAll( regexRootRelativeHref, `href="${ file.instance.locale.folder }/`  )
    }
    string = string.replaceAll( 'href="!', 'href="'  )
    //string = string.replaceAll( 'downloads.ctfassets.net', 'images.ctfassets.net'  )

    return string
  } catch ( error ) {
    logError(
      `  template: ${ file.template }`,
      `  id: ${ file.data.id }`,
      `  url: ${ file.data.url }`,
      error
    )
  }
}

/**
 * Outputs all routes
 */
const output = async () => {
  const keys = Object.keys( routes )
  await bluebird.map( keys, async route => {
    const html = await build( { route } )
    if ( route.indexOf( '.' ) === -1 ) {
      route += '.html'
    }
    route = route.replace( regexStartsWithSlash, '' )
    return fse.outputFile( path.resolve( settings.destination, route ), html )
  }, { concurrency: 100 } )
  const count = keys.length
  log( `  created ${ count } ${ count === 1 ? 'file' : 'files' }` )
}

const init = async _func => {
  _init = _func
  await refresh()
}

const refresh = async () => {
  if ( _init ) {
    routes = {}
    ids = {}
    try {
      settings = _init()
    } catch ( error ) {
      logError( `  error executing html init`, error )
      settings = null
      return
    }

    log( '', 'HTML' )
    for ( const instance of Object.values( instances ) ) {
      await prepare( instance )
    }

    const count = Object.keys( routes ).length
    log( `  prepared ${ count } ${ count === 1 ? 'route' : 'routes' }` )
  }
}

export default {
  init,
  refresh,
  output,
  build,
  exists
}
