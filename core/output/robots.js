import path from 'path'
import fse from 'fs-extra'
import log, { logError } from '../log.js'

export default async ( settings = {} ) => {
  settings.destination = settings.destination ?? './public/robots.txt'
  settings.allow = settings.allow ?? true
  settings.disallow = settings.disallow ?? null
  settings.noindex = settings.noindex ?? null
  settings.sitemaps = settings.sitemaps ?? null

  log( '', 'Robots' )

  if ( !settings.domain && settings.sitemap !== false ) {
    logError(
      '  Error creating robots.txt file',
      `  You must supply a value for 'domain' in the config.js`
    )
  }

  let file = 'User-agent: *'

  if ( settings.sitemap !== false ) {
    if ( settings.sitemaps?.length ) {
      settings.sitemaps.forEach( sitemap => {
        file += `\nSitemap: ${ settings.domain }${ sitemap }`
      } )
    } else {
      file += `\nSitemap: ${ settings.domain }/sitemap.xml`
    }
  }

  file += settings.allow ? '\nAllow:' : '\nDisallow: /'

  if ( settings.disallow ) {
    settings.disallow.forEach( path => {
      file += `\nDisallow: ${ path }`
    } )
  }

  if ( settings.noindex ) {
    settings.noindex.forEach( path => {
      file += `\nNoindex: ${ path }`
    } )
  }

  await fse.outputFile( path.join( process.cwd(), settings.destination ), file )

  log( `  destination: ${ settings.destination }` )
}
