import path from 'path'
import fse from 'fs-extra'
import log from '../log.js'
import js2xmlparser from 'js2xmlparser'

export default async ( settings = {} ) => {
  settings.destination = settings.destination ?? './public/sitemap.xml'
  settings.paths = settings.paths ?? []
  settings.folder = settings.folder ?? ''

  log( '', 'Sitemap' )

  if ( !settings.domain ) {
    logError(
      '  Error creating sitemap.xml file',
      `  You must supply a value for 'domain' in the config.js`
    )
  }

  const xml = { '@': { xmlns: 'http://www.sitemaps.org/schemas/sitemap/0.9' }, url: [] }
  settings.paths.forEach( ( group ) => {
    Object.values( group.items ).forEach( ( item ) => {
      xml.url.push(
        {
          loc: settings.domain + settings.folder + item.url,
          priority: group.priority
        }
      )
    } )
  } )

  const file = js2xmlparser.parse( 'urlset', xml, { declaration: { encoding: 'UTF-8' } } )

  await fse.outputFile( path.join( process.cwd(), settings.destination ), file )

  log( `  destination: ${ settings.destination }` )
}
