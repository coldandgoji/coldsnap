import path from 'path'
import fetch from 'node-fetch'
import fse from 'fs-extra'
import log, { logError } from '../log.js'
import { config } from '../../app.js'

export default async ( settings = {} ) => {
  settings.entries = settings.entries ?? []
  settings.schema = settings.schema ?? []
  settings.text = settings.text ?? []

  log( '' )
  log( 'Manifest' )

  // ensures there is a instance
  if ( !settings.instance ) {
    logError( "  You have not specified an 'instance'" )
  }

  // ensures there is an environment
  if ( !settings.environment ) {
    logError( "  You must provide a 'environment'" )
  }

  // ensures there is a locale
  if ( !settings.locale ) {
    logError( "  You must provide a 'locale'" )
  }

  // skips the manifest if there is no entry handling
  if ( !settings.entries || !settings.schema || settings.schema.length < 1 ) {
    log( '  Skipped' )
    return true
  }

  // creates the result data object
  const data = {
    context: {
      instanceId: settings.instance,
      environmentId: settings.environment,
    },
    locale: settings.locale,
    manifest: {
      schema: settings.schema,
      text: settings.text,
      entries: []
    }
  }

  // entry fields
  const fields = []
  settings.schema.forEach( field => {
    fields.push( Object.keys( field )[ 0 ] )
  } )

  // loops through each of the supplied search items
  // and builds the entries to be submitted
  // entries are built as arrays to reduce the
  // repetition of keys in the data
  settings.entries.forEach( item => {
    Object.values( item ).forEach( entry => {
      const entryData = settings.entry( entry )
      entry = []
      fields.forEach( field => {
        entry.push( entryData[ field ] )
      } )
      data.manifest.entries.push( entry )
    } )
  } )

  // outputs a summary of the data
  log(
    `  endpoint: ${ settings.endpoint }`,
    `  instance: ${ data.context.instanceId }`,
    `  environment: ${ data.context.environmentId }`,
    `  locale: ${ data.locale }`,
    `  entries: ${ data.manifest.entries?.length ?? 0 }`
  )

  // writes the manifest file locally
  if ( config.manifestlocal ) {
    log( `  saving to .coldsnap/manifest/...` )
    fse.ensureDirSync( './.coldsnap/manifest/' )
    fse.writeJsonSync( `./.coldsnap/manifest/${ data.context.instanceId }_manifest_${ data.locale }.json`, data )
  }

  // submits the manifest to the endpoint
  if ( !config.manifestlocal ) {
    log( `  sending...` )
    const response = await fetch( settings.endpoint, {
      method: 'POST',
      body: JSON.stringify( data ),
      headers: {
        'content-type': 'application/json',
        'x-api-key': settings.key
      }
    } )

    if ( response.ok ) {
      const text = await response.text()
      log( '  success', text )
    } else {
      logError( '  error', '', response.status, response.statusText )
    }
  }
}
