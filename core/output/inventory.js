import path from 'path'
import fse from 'fs-extra'
import log, { logError } from '../log.js'

export default async ( settings = {} ) => {
  settings.path = settings.path || './public/inventory.json'
  settings.entries = settings.entries || []

  log( '', 'Inventory' )

  if ( settings.entries.length && !settings.domain ) {
    logError(
      '  Error creating inventory.json file',
      `  You must supply a value for 'domain' in the config.js`
    )
  }

  // skips the inventory if there is no entry handling
  if ( settings.entries.length < 1 ) {
    log( '  Skipped' )
    return true
  }

  // creates the result data object
  const data = {
    entries: []
  }

  // loops through each of the supplied search items
  // and builds the entries to be submitted
  // items with a 'null' value are not output in
  // the resulting objects
  settings.entries.forEach( item => {
    Object.values( item ).forEach( entry => {
      data.entries.push( {
        id: entry.id,
        sku: entry.sku,
        type: entry.inventoryType ?? ( entry.addon ? 'addon' : 'product' ),
        name: entry.name,
        image: entry.imageUrl ? url( settings.domain, entry.imageUrl ) : null,
        url: url( settings.domain, entry.url ),
        price: entry.inventoryPrice || entry.finalPrice,
        measure: entry.measure,
        dimensions: {
          height: entry.height,
          length: entry.length,
          width: entry.width,
          weight: entry.weight
        },
        available: entry.inventoryAvailable,
        categories: entry.inventoryCategories ?? [],

        // new product fields
        description: entry.inventoryDescription,
        fields: entry.inventoryFields,
        attributes: entry.inventoryAttributes,
        group: entry.inventoryGroup,
        products: entry.inventoryProducts,
        stock: entry.inventoryStock,
        digital: entry.inventoryDigital,
        quantity: entry.inventoryQuantity,
        configurable: entry.inventoryConfigurable,
        balance: entry.inventoryBalance,

        // values necessary for 'addons'
        addons: entry.options.map( addon => addon.id )
      } )
    } )
  } )

  // writes the inventory file
  await fse.outputFile( path.join( process.cwd(), settings.path ), JSON.stringify( data ) )

  // determines the project path
  // so we can strip it from the logger output
  log( `  ${ settings.path }` )
}

const url = ( domain, _url ) => {
  if ( _url.indexOf( 'http' ) === 0 ) {
    return _url
  }
  if ( _url.indexOf( '//' ) === 0 ) {
    return 'https:' + _url
  }
  return domain + _url
}
