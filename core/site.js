import log, { logError } from './log.js'
import _ from 'lodash'

const remap = data => {
  data.title = data.siteTitle || data.title
  data.description = data.siteDescription || data.description
  delete data.siteTitle
  delete data.siteDescription
  return data
}

export default ( cs, data, types ) => {
  let site = _.merge( {}, cs.locale )
  const keys = _.keys( data.site )
  const key = site.siteDataId || keys[ 0 ]
  if ( key && !keys.includes( key ) ) {
    logError(
      `  Cannot find the site data for ${ key }`,
      '  Ensure the remote data is available or remove the siteDataId from the locale in config.js'
    )
  } else {
    site = _.merge( site, remap( _.get( data.site, `${ key }.data`, {} ) ) )
  }
  site.url = process.env.NODE_ENV === 'development' ? '/' : site.domain
  return types
    ? cs.factory.component( 'site', site )
    : site
}
