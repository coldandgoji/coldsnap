import _ from 'lodash'
import log, { logError } from './log.js'

/**
 * Factory to create instances of 'types'
 * It handles:
 *   - Keeping a single cached instance of each item
 *   - Creating instances from only an id within the model data
 */
export default class Factory {

  /**
   * Creates the factory instance
   */
  constructor ( cs ) {
    this.cs = cs
    this.types = null
    this.model = null
    this.logger = null
    this.cache = {}
    this.blacklist = {}
  }

  /**
   * Creates the initial instances from all available data items
   */
  initialize ( _types, _model ) {
    this.types = _types ?? null
    this.model = _model ?? null
    this.cache = {}
    this.blacklist = {}

    const data = {}
    log( '', 'Factory' )
    let count = 0
    Object.keys( this.types ).forEach( type => {
      if ( this.model[ type ] ) {
        data[ type ] = this.convert( this.model[ type ], type )
        count++
      }
    } )
    log( `  prepared ${ count } ${ count === 1 ? 'data type' : 'data types' }` )
    return data
  }

  /**
   * Generates a specific class with the data provided
   * This is used to generate class instances on the fly
   * while still respecting the ability for the class
   * itself to come from either the core or the site
   */
  generate ( classType, data ) {
    if ( !this.types[ classType ] ) {
      logError(
        'Failed to generate type instance',
        `Class: ${ classType }`,
        `Data: `,
        data
      )
    }
    return new this.types[ classType ]( data, this.cs )
  }

  /**
  * Makes an instance of a type class
  *
  * @param {String} contentId - The id of the class to create
  *   Also accepts an object containing an 'id' property
  * @param {String} classType - The string name of the class to create
  * @param {String} dataType - The data to check and build against
  *   When not supplied this defaults to the classType
  */
  make ( contentId, classType, dataType = null ) {
    // skips if no contentId was provided
    if ( !contentId ) {
      return null
    }

    // handles the submission of an object with an id (instead of a string id)
    contentId = ( typeof contentId === 'string' ) ? contentId : contentId.id

    // skips if a contentId still couldn't be resolved
    if ( !contentId ) {
      return null
    }

    // auto determines the class type when none was supplied
    if ( !classType ) {
      let keys = Object.keys( this.model )
      keys.some( ( key ) => {
        if ( this.model[ key ][ contentId ] ) {
          classType = key
          return true
        }
      } )
    }

    // skips the item if there is no class type
    if ( !classType ) {
      return null
    }

    // sets the dataType when none has been supplied
    if ( !dataType ) {
      dataType = classType
    }

    // if the data type itself doesn't exist, try to determine it
    // this occurs if a custom type extends 'page' but doesn't have its own data
    if ( !this.model[ dataType ] ) {
      let keys = Object.keys( this.model )
      keys.some( ( key ) => {
        if ( this.model[ key ][ contentId ] ) {
          dataType = key
          return true
        }
      } )
    }

    // skips if item if there is no data for the specified dataType (it's an invalid / broken reference)
    if ( !this.model[ dataType ][ contentId ] ) {
      return null
    }

    // creates the initial cache for this class type
    // also creates a blacklist on the first pass
    if ( !this.cache[ classType ] ) {
      this.cache[ classType ] = {}
    }

    // creates the initial blacklist for this class type
    // this is used to prevent re-creating instances that
    // have been skipped for the current locale
    if ( !this.blacklist[ classType ] ) {
      this.blacklist[ classType ] = {}
    }

    // skips the item if it is already in the blacklist
    if ( this.blacklist[ classType ][ contentId ] ) {
      return null
    }

    // creates the new class instance only if one doesn't already exists
    if ( !this.cache[ classType ][ contentId ] ) {
      // creates the instance
      let instance
      try {
        instance = new this.types[ classType ]( this.model[ dataType ][ contentId ], this.cs )
      } catch ( error ) {
        logError(
          'Failed to create type instance',
          `Class: ${ classType }`,
          `Model: ${ dataType }`,
          `Content Id: ${ contentId }`,
          '',
          error
        )
      }

      // ensures the instance is available for the current locale
      if ( instance.locale( this.cs.locale.type ) ) {
        this.cache[ classType ][ contentId ] = instance

      // stores the item in the locale blacklist
      // logs that the item was skipped
      // so the difference between locales is transparent
      } else {
        this.blacklist[ classType ][ contentId ] = true
        log( `  skipped! locale ${ this.cs.locale.code }, content: ${ classType } / ${ contentId } / ${ instance.data.title }` )
        return null
      }
    }

    // returns the instance
    return this.cache[ classType ][ contentId ]
  }

  /**
  * Wraps all the items in an array with the specified type class
  *
  * @param {Array} data - The data to wrap all items within
  *   Also accepts an object, but will convert it to an array
  * @param {String} classType - The string name of the type class to create
  * @param {String} dataType - The data to check and build against
  *   When not supplied this defaults to the classType
  */
  wrap ( data, classType, dataType = null ) {
    // wraps the item in an array if it is a string
    if ( typeof data === 'string' ) {
      data = [ data ]
    }

    // ensures the supplied value is an array
    let values = ( data ) ? Object.values( data ) : []

    // creates the empty array representing the wrapped items
    let returnTypes = []

    // creates an empty placeholder for the instances
    let madeInstance = null

    // loops through each item in the supplied data
    values.forEach( item => {
      // only processes if the item is truthy
      if ( item ) {
        // skips creating the item
        // as it is already a component instance created elsewhere
        if ( item.contentType && item.ssg && ( !classType || classType === item.contentType ) ) {
          madeInstance = item

        // attempts to create a new class instance
        // checking it against the available remote/local data
        } else {
          madeInstance = this.make( item, classType, dataType )
        }

        // attempts to generate a runtime instance instead
        // this allows us to 'wrap' arrays of runtime generated
        // type instances that do not exist in the remote/local data
        if ( !madeInstance && item && _.isPlainObject( item ) && Object.keys( item ).length ) {
          // attempts to determine the classType from the object
          if ( !classType ) {
            let autoClassType = Object.keys( item )[ 0 ]
            let autoItem = item[ autoClassType ]
            madeInstance = this.component( autoClassType, autoItem )

          // generates the specified classType supplying the available data
          } else {
            madeInstance = this.component( classType, item )
          }
        }

        // adds the instance to the array if it was valid
        if ( madeInstance ) {
          returnTypes.push( madeInstance )

        // warns when an item could not be made
        // this is usually due to an invalid reference in the data
        } else {
          // log( 'Factory.wrap: WARNING: Item not made: ' + classType + ' ' + item );
        }

      // warns when an invalid item is in the supplied array
      } else {
        // log( 'Factory.wrap: WARNING: Undefined item in array: ' + classType + ' ' + item );
      }
    } )

    // returns all of the instances that were created
    return returnTypes
  }

  /**
  * Converts all the items in an object to the type instance
  * Under most circumstances 'wrap' should be used
  * As an 'array' result is normally required
  *
  * @param {Object} data - The data to convert all items within
  * @param {String} classType - The string name of the type class to create
  * @param {String} dataType - The data to check and build against
  *   When not supplied this defaults to the classType
  */
  convert ( data, classType, dataType = null ) {
    // extracts the keys from the supplied object
    let keys = ( data ) ? Object.keys( data ) : []

    // creates the empty object representing the converted items
    let returnTypes = {}

    // creates an empty placeholder for the instances
    let madeInstance = null

    // loops through each key in the supplied data
    keys.forEach( ( key ) => {
      // only processes if the item is truthy
      if ( data[ key ] ) {
        // attempts to create a class instance
        madeInstance = this.make( data[ key ], classType )

        // adds the instance to the array if it was valid
        if ( madeInstance ) {
          returnTypes[ key ] = madeInstance

        // warns when an item could not be made
        // this is usually due to an invalid reference in the data
        } else {
          // log( 'Factory.convert: WARNING: Item not made: ' + classType + ' ' + item );
        }

      // warns when an invalid item is in the supplied array
      } else {
        // log( 'Factory.convert: WARNING: Undefined item in array: ' + classType + ' ' + item );
      }
    } )

    // returns all of the instances that were created
    return returnTypes
  }

  component ( component, data = null ) {
    let modifiers = []

    // ensures a string has been supplied
    if ( typeof component !== 'string' ) {
      logError(
        'factory.component called without supplying a string',
        '',
        'Component: ',
        component,
        '',
        'Data: ',
        data
      )
    }

    // removes spaces from the component
    component = component.replace( /\s/g, '' )

    // removes a starting period from the component
    component = component.replace( /^\./, '' )

    // determines if any modifiers are in effect
    if ( component.indexOf( '.' ) !== -1 ) {
      modifiers = component.split( '.' )
      component = modifiers.shift()
    }

    // ensures the data object isn't undefined/null
    if ( data === undefined || data === null ) {
      data = {}
    }

    // returns the item as a list
    if ( Array.isArray( data ) ) {
      return this.generate( 'list', { items: data } )
    }

    // returns the existing instance
    // when there are no modifiers or overrides
    if ( data.ssg && data.contentType && data.contentType === component && modifiers.length === 0 ) {
      return data
    }

    // assumes the new component will have no data
    let settings = {}

    // merges in the data of the existing component
    if ( data.ssg ) {
      settings = _.assign( settings, data.data )

    // merges in the data directly
    } else {
      settings = _.assign( settings, data )
    }

    // merges in the modifiers
    if ( modifiers.length ) {
      settings = _.assign( settings, { modifiers: modifiers } )
    }

    return this.generate( component, settings )
  }
}
