import path from 'path'
import { DEVELOP, iteration } from '../app.js'

export default async file => {
  if ( file.indexOf( './' ) === 0 ) {
    file = path.resolve( './source/types/', file )
  }
  file = DEVELOP
    ? `${file}#${ iteration }`
    : file

  let instance = null

  try {
    instance = ( await import( file ) ).default
  } catch( error ) {
    // TODO: Remove once the error thrown by module loading is more useful
    //       https://github.com/nodejs/modules/issues/471
    //       https://github.com/mochajs/mocha/blob/master/lib/esm-utils.js#L14
    if (
      error instanceof SyntaxError &&
      error.message &&
      error.stack &&
      !error.stack.includes( file )
    ) {
      const errorNew = new SyntaxError( error.message );
      errorNew.stack = error.stack.replace(
        /^SyntaxError/,
        `SyntaxError\n    at ${file}`
      )
      throw errorNew
    } else {
      throw error
    }
  }

  return instance
}
