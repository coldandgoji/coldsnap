import colors from 'colors/safe.js'
import server from './server.js'
import { BUILD, DEVELOP } from '../app.js'

const start = process.hrtime.bigint()

const benchmark = () => {
  const elapsed =  Number( process.hrtime.bigint() - start ) / 1000000
  const seconds = Math.floor( elapsed / 1000 )
  const value = seconds + ' s, ' + ( ( elapsed - ( seconds * 1000 ) ).toFixed() ).padStart( 3, '0' ) + ' ms'
  return value.padEnd( 15 )
}

export default ( ...args ) => {
  const lines = [ ...args ]
  lines.forEach( line => {
    console.log( benchmark(), line )
  } )
}

export const logError = ( ...args ) => {
  // console
  const linesConsole = [ ...args ]
  linesConsole.forEach( line => {
    console.log( benchmark(), colors.red( line ) )
  } )

  // server
  if ( DEVELOP ) {
    const lines = [ ...args ]
    const linesServer = []
    lines.forEach( line => {
      if ( typeof line === 'object' ) {
        if ( line.message ) {
          linesServer.push( line.message )
        }
        if ( line.stack ) {
          linesServer.push( line.stack )
        }
      } else {
        linesServer.push( line )
      }
    } )
    server.error( '<div style="color:#fc6559;white-space:pre-wrap;">' + linesServer.join( '<br>' ) + '</div>' )
  }

  // fails the build
  if ( BUILD ) {
    process.exit( 1 )
  }
}
