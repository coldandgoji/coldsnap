import log, { logError } from './core/log.js'

import _ from 'lodash'
import fse from 'fs-extra'

import configuration from './core/configuration.js'
import Factory from './core/factory.js'
import server from './core/server.js'
import { default as siteInit } from './core/site.js'
import { default as typeLoader } from './core/input/types.js'
import html from './core/output/html.js'
import path, { dirname } from 'path'
import { fileURLToPath } from 'url'

const __filename = fileURLToPath( import.meta.url )
const __dirname = dirname( __filename )

process.on( 'unhandledRejection', ( reason, p ) => {
  logError( 'Unhandled Rejection', reason )
  if ( !config || !config.mode || config.mode !== 'develop' ) {
    process.exit( 1 )
  }
} )

export let BUILD = false
export let DEVELOP = false
export let PREVIEW = false

export let instances = {}

export let config = null
export let locale = null
export let locales = null
export let projectId = null
export let site = null
export let data = null

export let iteration = 1

let _types = null
let _settingsPopulate = null
let init = false

export default async _config => {
  if ( init ) process.exit( 0 )
  init = true

  const packageString = await fse.readFile( path.join( __dirname, './package.json' ), 'utf8' )
  const packageJson = JSON.parse( packageString )
  log( `Coldsnap v${ packageJson.version }` )

  ;( { config, locale, locales } = configuration( _config ) )

  BUILD = config.mode === 'build'
  DEVELOP = config.mode === 'develop'
  PREVIEW = !!config.preview

  projectId = locale.projectId

  _.each( locales, ( _locale, key ) => {
    const instance = {
      config,
      locale: _locale,
      log,
      projectId: _locale.projectId
    }
    if ( _config?.utilities ) {
      instance.util = _config.utilities( _locale )
    }
    instance.factory = new Factory( instance )
    instances[ key ] = instance
  } )
}

export { default as _ } from 'lodash'

export { default as importer } from './core/importer.js'
export { default as pipeline } from './core/pipeline.js'
export { server }

export { default as firebase } from './core/input/firebase.js'
export { default as local } from './core/input/local.js'
export { default as templates } from './core/input/templates.js'

export { default as copy } from './core/output/copy.js'
export { html }
export { default as inventory } from './core/output/inventory.js'
export { default as manifest } from './core/output/manifest.js'
export { default as robots } from './core/output/robots.js'
export { default as sitemap } from './core/output/sitemap.js'

export { log, logError }

export const iterate = () => {
  iteration++
}

export const populate = async settings => {
  _settingsPopulate = settings ?? _settingsPopulate

  for ( const instance of Object.values( instances ) ) {
    let _data = _types
      ? instance.factory.initialize( _types, _settingsPopulate.data )
      : _settingsPopulate.data

    // sets the instance values
    instance.site = siteInit( instance, _data, _types )
    instance.data = _data

    // sets the root values for the active instance
    // this provides a bit of backwards compatibilty
    // making the config for single instance sites cleaner
    if ( locale.type === instance.locale.type ) {
      data = instance.data
      site = instance.site
    }

    // performs the optional data manipulation
    if ( _settingsPopulate.manipulate ) {
      instance.data = await _settingsPopulate.manipulate( _data, instance )
    }
  }

  await html.refresh()
  server.refresh()
}

export const types = async typeset => {
  _types = await typeLoader( {
    types: typeset,
    onChange: _data => {
      _types = _data
      populate()
    }
  } )
}

export const remove = async path => {
  log(
    '',
    'Remove',
    `  path: ${ path }`
  )
  return fse.remove( path )
}
